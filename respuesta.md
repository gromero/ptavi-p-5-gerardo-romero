# Protocolos para la transmisión de audio y video en Internet
# Práctica 5. Sesión SIP

## Ejercicio 3. Análisis general

* ¿Cuántos paquetes componen la captura?    
	1050 paquetes  
* ¿Cuánto tiempo dura la captura?  
	10.52 segundos  
* ¿Qué IP tiene la máquina donde se ha efectuado la captura?  
	192.168.1.116  
* ¿Se trata de una IP pública o de una IP privada?  
	IP privada  
* ¿Por qué lo sabes?  
	Es una ip privada por que tiene la numerologia caracterirstica de las redes privadas, empezando por 192.168, que es el que utilizan las redes privadas  

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.  

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?  
	SIP con un 1.2%(13) y RTP con 98.2%(1031) de paquetes en el nivel de aplicación  
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?  
	Ethernet IP UDP ICMP RTP  
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?  
	RTP con 134kb/sec  

Observa por encima el flujo de tramas en el menú de `Statistics` en `IO Graphs`. La captura que estamos viendo corresponde con una llamada SIP.  

Filtra por `sip` para conocer cuándo se envían paquetes SIP, o por 'rtp', para conocer cuándo se envían los RTP.  

* ¿En qué segundos tienen lugar los dos primeros envíos SIP?  
	0.000012938 y 0.063305950  
* Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?  
	paquete 6, segundo 0.174851163  
* Los paquetes RTP, ¿cada cuánto se envían?  
	Basta con restar dos envios simultaneos, todos presentan la misma proporcion, por lo tanto, cada (0.187103576 - 0.174851163 = 0,01 segundos aproximadamente.  

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).  

Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.  

##Ejercicio 4. Primeras tramas  

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y "Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.  

* ¿De qué protocolo de nivel de aplicación son?  
	Protocolo SIP  
* ¿Cuál es la dirección IP de la máquina "Linphone"?  
	192.168.1.116  
* ¿Cuál es la dirección IP de la máquina "Servidor"?  
	212.79.111.155  
* ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
	Se intenta establecer una conexión entre el cliente y servidor a traves del invite del cliente  
* ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?  
	El servidor recive el invite del cliente y analiza la información con un trying.  

Ahora, veamos las dos tramas siguientes.  

* ¿De qué protocolo de nivel de aplicación son?  
	Protocolo SIP  
* ¿Entre qué máquinas se envía cada trama?  
	Entre cliente 192.168.1.116 y servidor 212.79.111.155  
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?  
	El servidor con un ok acepta al cliente para establecer la conexión.  
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?  
	Se manda un ack al servidor para avisar de que la conexión está establecida.  

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).  
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.  

## Ejercicio 5. Tramas finales  

Después de la trama 250, busca la primera trama SIP.  

* ¿Qué número de trama es?  
	Trama 1042  
* ¿De qué máquina a qué máquina va?  
	Del cliente al servidor  
* ¿Para qué sirve?  
	Para informar que quiere cerrar la conexión al servidor   
* ¿Puedes localizar en ella qué versión de Linphone se está usando?  
	Se puede ver dentro de "User Agent" en "Message Header", es limphone Desktop/4.3.2  

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.  

## Ejercicio 6. Invitación

Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)  

* ¿Cuál es la direccion SIP con la que se quiere establecer una llamada?  
	sip:music@sip.iptel.org  
* ¿Qué instrucciones SIP entiende el UA?  
	Se ven dentro de Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE  

* ¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?  
	Content-Type  
* ¿Cuál es el nombre de la sesión SIP?  
	Talk  


## Ejercicio 7. Indicación de comienzo de conversación

En la propuesta SDP de Linphone puede verse un campo `m` con un valor que empieza por `audio 7078`.  
 
* ¿Qué trama lleva esta propuesta?  
	Trama 2  
* ¿Qué indica el `7078`?  
	El puerto UDP desde el que se manda el audio  
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?  
	que será el puerto por donde se reciban los paquetes inclusive de un protocolo distinto.  
* ¿Qué paquetes son esos?  
	RTP 2, 139, 193  

En la respuesta a esta propuesta vemos un campo `m` con un valor que empieza por `audio XXX`.  

* ¿Qué trama lleva esta respuesta?  
	trama 3  
* ¿Qué valor es el `XXX`?  
	29448  
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?  
	que será el puerto por donde se reciban los paquetes inclusive de un protocolo distinto.  
* ¿Qué paquetes son esos?


Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.  

## Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

* ¿De qué máquina a qué máquina va?   
	192.168.1.116 a 212.79.111.155 cliente-servidor  
* ¿Qué tipo de datos transporta?  
	audio  
* ¿Qué tamaño tiene?  
	214 bytes  
* ¿Cuántos bits van en la "carga de pago" (payload)  
	se ve en el protocolo udp 172 bytes  
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?  
	aproximadamente 0,01 segundos  

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.  

## Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?  
	Dos flujos , se ve dentro de flujos no de stream   
* ¿Cuántos paquetes se pierden?  
	0  
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
	30.726700 ms  
* ¿Qué es lo que significa el valor de delta?  
	Es el valor máximo en mili segundos que ha habido entre un paquete y el consecutivo entre todos los que hay.  
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
	En el flujo de servidor a cliente encontramos los valores maximos, 7.110079 para Max Jitter y 2.995904 para Mean Jitter  
* ¿Qué significan esos valores?
	El Max Jitter es el retardo máximo que hay ente paquetes en un flujo determinado, y el Mean Jitter es el flujo medio entre todos los paquetes en un flujo determinado.  
Vamos a ver ahora los valores de una trama concreto, la númro 27. Vamos a analizarla en opción `Telephony`, `RTP`, `RTP Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?  
	Esta en el flujo serv-client, Delta vale 0.000164 y Jitter 3.087182  
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?  
	En skew se ve  
* El "skew" es negativo, ¿qué quiere decir eso?  
	Que ha habido un retraso en el paquete  

En el panel `Stream Analysis` puedes hacer `play` sobre los streams:  

* ¿Qué se oye al pulsar `play`?  
	Un audio  
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?  
	un audio con mala calidad  
* ¿A qué se debe la diferencia?  
	al tamaño del buffer, al ser tan pequeño no se escucha como debería  


Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.
  
## Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` selecciona el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?

Ahora, pulsa sobre `Flow Sequence`:

* Guarda el diagrama en un fichero (formato PNG) con el nombre `diagrama.png`.
* ¿En qué segundo se recibe el último OK que marca el final de la llamada?
  
Ahora, selecciona los dos streams, y pulsa sobre `Play Sterams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
* ¿Cuál es la frecuencia de muestreo del audio?
* ¿Qué formato se usa para los paquetes de audio (payload)?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 11. Captura de una llamada VoIP

Desde LinPhone, créate una cuenta SIP.  A continuación:

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 10 segundos de duración. Comienza a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Asegura (usando los filtros antes de guardarla) que en la captura haya solo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos RTP tiene esta captura?
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que hemos indicado (como fork del repositorio plantilla que os proporcionamos). Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:

* Se valorará que haya realizado al menos haya ocho commits, correspondientes más o menos con los ejercicios pedidos, en al menos dos días diferentes, sobre la rama principal del repositorio.
* Se valorará que el fichero de respuestas esté en formato Markdown correcto.
* Se valorará que esté el texto de todas las preguntas en el fichero de respuestas, tal y como se indica al principio de este enunciado.
* Se valorará que las respuestas sean fáciles de entender, y estén correctamente relacionadas con las preguntas.
* Se valorará que la captura que se pide tenga exactamente lo que se pide.
* Se valorará que el fichero con el diagrama de la llamada VoIP tenga solo ese diagrama, en el formato que se indica en el enunciado.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para los archivos son los mismos que indica el enunciado.

## ¿Cómo puedo probar esta práctica?

Cuando tengas la práctica lista, puedes realizar una prueba general, de que los ficheros en el directorio de entrega son los adecuados, y alguna otra comprobación. Para ello, ejecuta el archivo `check.py`, bien en PyCharm, o bien desde la línea de comandos:

```shell
python3 check.py
```

## Ejercicio 12 (segundo periodo). Llamada LinPhone

Ponte de acuerdo con algún compañero para realizar una llamada SIP conjunta (entre los dos) usando LinPhone, de unos 15 segundos de duración.  Si quieres, usa el foro de la asignatura, en el hilo correspondiente a este ejercicio, para enconrar con quién hacer la llamada.

Realiza una captura de esa llamada en la que estén todos los paquetes SIP y RTP (y sólo los paquetes SIP y RTP). No hace falta que tu compañero realice captura (pero puede realizarla si quiere que le sirva también para este ejercicio).

Guarda tu captura en el fichero `captura-a-dos.pcapng`

Como respuesta a este ejercicio, indica con quién has realizado la llamada, quién de los dos la ha iniciado, y cuantos paquetes hay en la captura que has realizado.
